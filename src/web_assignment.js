var information = [
    {
        role: "Software Developer & Programer" ,
        jobsnumber : 590 ,
        max : 100,
        min: 72,
        image:"Software_Developers_&_Programmers.jpg",
        des:"Software developers and programmers develop and maintain computer software, websites and software applications (apps).",
        skill:[
            "* Computer Software and Systems",
            "* Programming Languages and Techniques",
            "* Software Development Processes such as Agile",
            "* Confidentiality, Data Security and Data Protection Issues"
        ]
    },
    {
        role: "Database & Systems Administration",
        jobsnumber: 74,
        max: 90,
        min: 66,
        image:"Database_&_Systems_Administrators.jpg",
        des:"Database & systems administrators develop, maintain and administer computer operating systems, database management systems, and security policies and procedures.",
        skill:[
            "* A range of Database Technologies and Operating Systems",
            "* New Developments in Databases and Security Systems",
            "* Computer and Database Principles and Protocols",
        ]
    },
    {
        role: "Help Desk & IT Support",
        jobsnumber: 143,
        max: 65,
        min: 46,
        image:"Help_Desk_&_IT_Support.jpg",
        des:"Information technology (IT) helpdesk/support technicians set up computer and other IT equipment and help prevent, identify and fix problems with IT hardware and software.",
        skill:[
            "* Computer Hardware, Software, Networks and Websites",
            "* The Latest Developments in Information Technology",
        ]
    },
    {
        role: "Data Analyst",
        jobsnumber: 270,
        max: 128,
        min: 69,
        image:"Data_Analyst.jpg",
        des:"Data analysts identify and communicate trends in data using statistics and specialised software to help organisations achieve their business aims.",
        skill:[
            "* Data Analysis Tools such as Excel, SQL, SAP and Oracle, SAS or R",
            "* Data Analysis, Mapping and Modelling Techniques",
            "* Analytical Techniques such as Data Mining",
        ]
    },
    {
        role: "Test Analyst",
        jobsnumber: 127,
        max: 98,
        min: 70,
        image:"Test_Analyst.jpg",
        des:"Test analysts design and carry out testing processes for new and upgraded computer software and systems, analyse the results, and identify and report problems.",
        skill:[
            "* Programming Methods and Technology",
            "* Computer Software and Systems",
            "* Project Management",
        ]
    },
    {
        role: "Project Management",
        jobsnumber: 188,
        max: 190,
        min: 110,
        image:"Project_Management.jpg",
        des:"Project managers use various methods to keep project teams on track. They also help remove obstacles to progress.",
        skill:[
            "* Principles of Project Management",
            "* Approaches and Techniques such as Kanban and Continuous Testing",
            "* How to Handle Software Development Issues",
            "* Common Web Technologies Used by the Scrum Team."
        ]
    }
];

$(function () {
    creatTable();
    clickChange();
});
//Create the table
function creatTable() {
    for (var i = 0; i < information.length; i++) {
        var tb = $("#maintable").append("<tr>"
            + "<td>" + information[i].role + "</td>"
            + "<td>" + information[i].jobsnumber + "</td>"
            + "<td>" + information[i].max + "</td>"
            + "<td>" + information[i].min + "</td>"
        );

    }
    num = information.length;
    var total=0;
    var totalMax=0;
    var totalMin=0;
    for (var i = 0; i < num; i++) {
        total += information[i].jobsnumber;
        totalMax += information[i].max;
        totalMin += information[i].min;
    }
    avgMax = Math.floor(totalMax / num);
    avgMin = Math.floor(totalMin / num);
    $("#sumandavg").append("<tr><td></td>"
        + "<td>Total:" + total + "</td>"
        + "<td>Average:" + avgMax + "</td>"
        + "<td>Average:" + avgMin + "</td>"
        + "</tr>"
    );
}
//Change job's information
function clickChange() {
    $(document).on("click", "tr", function () {
        var role = this.firstChild.textContent;
        for (var j = 0; j < information.length; j++) {
            if (role == information[j].role) {
                $(".detail")[0].src = "../src/images/" + information[j].image;
                $("span")[1].textContent = information[j].role;
                $(".detail")[2].textContent = information[j].des;
                $("dd").hide();
                for (var k = 0; k < information[j].skill.length; k++) {
                    var skills = $("dl").append("<dd>" + information[j].skill[k] + "</dd>");
                }
            }
        }

    });
}
